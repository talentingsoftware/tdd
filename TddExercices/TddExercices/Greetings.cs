﻿using System.Collections.Generic;

namespace TddExercices
{
   public class Greetings
    {
        public static string Greet(string name)
        {
            if (name.Length == 0)
            {
                return "Hello, my friend.";
            }

            if (IsUppercase(name))
            {
                return "HELLO, " + name + "!";
            }

            return "Hello, " + name + ".";
        }

        public static string Greet(string firstName, string secondName)
        {
            return "Hello, " + firstName + " and " + secondName+".";
        }

        public static string Greet(List<string> names)
        {
            List<string> lowercaseElements = new List<string>();
            List<string> uppercaseElements = new List<string>();

            foreach (var name in names)
            {
                if (IsUppercase(name))
                {
                    uppercaseElements.Add(name);
                }
                else lowercaseElements.Add(name);
            }

            string message = "Hello, ";

            if (uppercaseElements.Count == 0)
            {
                foreach (var name in names)
                {
                    if (name == names[names.Count - 1])
                    {
                        message += "and " + name + ".";
                    }
                    else message += name + ", ";
                }
            }
            else
            {
                foreach (var element in lowercaseElements)
                {
                    if (element == lowercaseElements[lowercaseElements.Count - 1])
                    {
                        message += "and " + element + ".";
                    }
                    else message += element + ", ";
                }

                message += " AND HELLO ";
                foreach (var element in uppercaseElements)
                {
                    if (uppercaseElements.Count == 1)
                    {
                        message += element + "!";
                    }
                    else if (element == uppercaseElements[uppercaseElements.Count - 1])
                    {
                        message += element + "!";
                    }
                    else message += element + ", ";
                }
            }
            return message;
        }
        public static bool IsUppercase(string name)
        {
            if (name.Equals(name.ToUpper())) return true;
            return false;
        }

    }
}
