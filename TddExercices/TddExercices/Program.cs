﻿using System;

namespace TddExercices
{
    class Program
    {
        static void Main(string[] args)
        {
            string address = @"..\..\Names.txt";
            NamesValidator namesValidator = new NamesValidator();
            try
            {
                namesValidator.Read(address);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            Console.ReadKey();
        }
    }
}
