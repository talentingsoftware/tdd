﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace TddExercices
{
    public class NamesValidator
    {
        public List<string> _input;

        public NamesValidator()
        {
            _input = new List<string>();
        }
        public void Command()
        {
          
            if (_input.Count == 1)
                Console.WriteLine(Greetings.Greet(_input.ElementAt(0)));
            else if (_input.Count == 0)
                Console.WriteLine(Greetings.Greet(""));
            else if (_input.Count == 2)
                Console.WriteLine(Greetings.Greet(_input.ElementAt(0), _input.ElementAt(1)));
            else
                Console.WriteLine(Greetings.Greet(_input));
        }
        public void Filter(string[] lines)
        {
           
            foreach (string line in lines)
            {
                if (line.Contains(",") && !line.Contains('"'))
                {
                    string[] elements = line.Trim().Split(',', ' ');
                    foreach (var element in elements)
                    {
                        if (HasOnlyLetters(element))
                        {
                            _input.Add(element);
                        }

                    }
                }
                else if (!HasOnlyLetters(line))
                {
                    var onlyLetters = new String(line.Where(item => Char.IsLetter(item) || Char.IsWhiteSpace(item) || item == ',').ToArray());
                    _input.Add(onlyLetters);
                }
                else
                {
                    _input.Add(line);
                }
            }
        }
        public void Read(string adress)
        {

            if (adress == "" || adress == null)
            {
                throw new Exception("The given adress is empty.");
            }
            if (!File.Exists(adress))
            {
                throw new Exception("The given adress does not contain a file.");
            }
            string[] lines = System.IO.File.ReadAllLines(adress);
            Filter(lines);
            Command();
        }

        public bool IsNull(string text)
        {
            if (text == "" || text == null)
            {
                return true;
            }
            return false;
        }

        public bool HasOnlyLetters(string text)
        {
            if (text.All(Char.IsLetter) && text!="")
            {
                return true;
            }
            return false;
        }
    }
}
