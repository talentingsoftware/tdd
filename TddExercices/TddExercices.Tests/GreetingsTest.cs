﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TddExercices.Tests
{
    class GreetingsTest
    {
        private List<string> names;
        [Test]
        [TestCase ("")]
        [TestCase("abc")]
        public void Read_ShouldThrownAnException(string address)
        {
            NamesValidator namesValidator = new NamesValidator();
            Assert.Throws<Exception>(() => namesValidator.Read(address));
        }

        //For exercise 7
        [Test]
        public void Filter_SuccesFirstCase()
        {
            string[] lines = { "Victor", "Elena, Dan" };
            NamesValidator namesValidator = new NamesValidator();
            List<string> expectedResult = new List<string>();
            expectedResult.Add("Victor");
            expectedResult.Add("Elena");
            expectedResult.Add("Dan");
            namesValidator.Filter(lines);
            Assert.That(expectedResult, Is.EqualTo(namesValidator._input));
        }

        //For exercise 8
        [Test]
        public void Filter_SuccesSecondCase()
        {
            string[] lines = { "Alina", "\"George, Dan\"" };
            NamesValidator namesValidator = new NamesValidator();
            List<string> expectedResult = new List<string>();
            expectedResult.Add("Alina");
            expectedResult.Add("George, Dan");
            namesValidator.Filter(lines);
            Assert.That(expectedResult, Is.EqualTo(namesValidator._input));
        }

        [Test]
        public void Filter_SuccesThirdCase()
        {
            string[] lines = { "Alina", "George" };
            NamesValidator namesValidator = new NamesValidator();
            List<string> expectedResult = new List<string>();
            expectedResult.Add("Alina");
            expectedResult.Add("George");
            namesValidator.Filter(lines);
            Assert.That(expectedResult, Is.EqualTo(namesValidator._input));
        }

        [Test]
        [TestCase ("", true)]
        [TestCase("a", false)]

        public void IsNullShould(string text, bool expectedResult)
        {
            NamesValidator namesValidator = new NamesValidator();
            bool actualResult = namesValidator.IsNull(text);
            Assert.That(actualResult, Is.EqualTo(expectedResult));

        }

        [Test]
        [TestCase("isNotUpper", false)]
        [TestCase("UPPER", true)]
        public void IsUppercaseShould(string text, bool expectedResult)
        {
            bool actualResult = Greetings.IsUppercase(text);
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

        //Exercise 1, 2 and 3
        [Test]
        [TestCase("Mihai", ExpectedResult = "Hello, Mihai.")]
        [TestCase("MIHAI", ExpectedResult = "HELLO, MIHAI!")]
        [TestCase("", ExpectedResult = "Hello, my friend.")]
        public string GreetWithOneNameShould(string text)
        {
            return Greetings.Greet(text);
        }

        //exercise 4
        [Test]
        [TestCase("Mihai", "Andrei", ExpectedResult = "Hello, Mihai and Andrei.")]
        [TestCase("Ionut", "Bogdan", ExpectedResult = "Hello, Ionut and Bogdan.")]
        public string GreetWithTwoNameShould(string firstName, string secondName)
        {
            return Greetings.Greet(firstName, secondName);
        }

        //exercise 5
        [Test]
        public void GreetWithNamesSucces()
        {
            names = new List<string>();
            names.Add("Ana");
            names.Add("Maria");
            names.Add("Alina");
            string expectedResult = "Hello, Ana, Maria, and Alina.";
            Assert.That(expectedResult, Is.EqualTo(Greetings.Greet(names)));
        }

        //exercise 6
        [Test]
        public void GreetWithNamesUpperCaseSucces()
        {
            names = new List<string>();
            names.Add("Ana");
            names.Add("MARIA");
            names.Add("Alina");
            string expectedResult = "Hello, Ana, and Alina. AND HELLO MARIA!";
            Assert.That(expectedResult, Is.EqualTo(Greetings.Greet(names)));
        }

        [Test]
        [TestCase("1234a", false)]
        [TestCase("_!$#$4a", false)]
        [TestCase("HasOnlyLetters", true)]
        public void HasOnlyLettersShould(string text, bool expectedResult)
        {
            NamesValidator namesValidator = new NamesValidator();
            bool actualResult = namesValidator.HasOnlyLetters(text);
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

    }
}
